#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

Fri 11 May 2018 02:36:54 PM CEST

"""

# Main classification dictionary
classes_dict = {
    'CE': ['Geng 2012', 'Ghisellini 2015', 'Haas 2015', 'Su 2013', 'EEA 2016'],
    'Gen': [],
}

# Legend of classes
legend = {
    'CE': 'Circular Economy',
    'Gen': 'Gender',
}

CE = classes_dict['CE']

# Extra stop-words for tag-cloud
extra_STOPWORDS = [
    "per",
    "mean",
    "data",
    "rate",
    "total",
    "percentage",
    "average",
    "number",
    "row",
    "ratio",
    "unit",
    "related",
]

