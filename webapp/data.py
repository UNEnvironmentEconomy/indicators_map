#!/usr/bin/env python
# -*- coding:utf -*-
"""
#Created by Esteban.

Sun 21 Jan 2018 01:15:06 PM CET

"""

import os
from os import path
import time
import pandas as pd
# import pandas.io.sql as pd_sql
# import sqlite3 as sql
from sqlalchemy import create_engine
import gspread
from oauth2client.service_account import ServiceAccountCredentials


class gdata():
    """Main class for handling gdata."""

    def __init__(self,
            file_name = "Indicator matching",
            credentials = "credentials.json",
            db_name = "data.sqlite3",
            tail = -650,
            sql_table_name = 'indicators',
                 ):
        self.file_name = file_name
        self.cf = credentials
        self.tail = tail
        self.fn_db = os.path.join(os.path.dirname(__file__), db_name)
        self.engine = create_engine('sqlite:///{}'.format(self.fn_db))
        self.sql_table_name = sql_table_name
        self.credentials_file = path.join(os.getcwd(), credentials)
        self.has_gdf = False
        self.has_credentials = False

    def _get_credentials(self):
        print('using {} as credentials'.format(self.credentials_file))
        scope = ['https://spreadsheets.google.com/feeds']
        credentials = ServiceAccountCredentials.from_json_keyfile_name(
            self.credentials_file, scope)
        self.gc = gspread.authorize(credentials)

    def _get_gdata(self, worksheet=0, start_row=1):
        """Get data from google docs."""
        if not self.has_credentials:
            self._get_credentials()
        print("Fetching data from gdoc", end="... ")
        start = time.clock()
        wks = self.gc.open(self.file_name).get_worksheet(worksheet)
        dataframe = pd.DataFrame(wks.get_all_records())
        self.gdf = dataframe#.iloc[0:self.tail, start_row:]
        self.has_gdf = True
        elapsed = time.clock(); print("OK  {0:0.2f}s".format(elapsed - start))

    def populate_sql(self):
        print("Populating database ", self.fn_db)
        if not self.has_gdf:
            self._get_gdata()
        # con = sql.connect(self.fn_db)
        self.gdf.to_sql(self.sql_table_name, self.engine, if_exists='replace')
        # con.close()

    def get_sqldata(self):
        print("Reading data from sqlite database ", self.fn_db)
        with self.engine.connect() as conn, conn.begin():
            self.sqldf = pd.read_sql_table(self.sql_table_name, conn)


def main():
    gd = gdata()
    # gd.get_sqldata()
    # print(gd.sqldf.head())
    gd.populate_sql()
    print(gd.gdf)


if __name__ == "__main__":
    main()
